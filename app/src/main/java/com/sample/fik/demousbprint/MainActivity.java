package com.sample.fik.demousbprint;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.usbsdk.UsbController;

import java.io.OutputStream;

public class MainActivity extends BaseActivity {

    private OutputStream mmOutputStream;
    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
    private Button btnSend = null,bt_btn_conn,bt_btnSend;
    private EditText txt_content = null;

    private int[][] u_infor;
    UsbController usbCtrl = null;
    UsbDevice dev = null;
    private Button btn_conn;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    public String SelectedBDAddress;
    public EditText etBT;

    private static final int REQUEST_ENABLE_BT = 2;
    BluetoothService mService = null;
    BluetoothDevice con_dev = null;
    private static final int REQUEST_CONNECT_DEVICE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_conn    = (Button) findViewById(R.id.btn_conn);
        btnSend     = (Button) findViewById(R.id.btnSend);
        bt_btn_conn = (Button) findViewById(R.id.btn_connbt);
        bt_btnSend  = (Button) findViewById(R.id.btnSendbt);
        etBT        = (EditText) findViewById(R.id.txt_contentbt);

        txt_content = (EditText) findViewById(R.id.txt_content);

        mService = new BluetoothService(this, mHandler2);
        //À¶ÑÀ²»¿ÉÓÃÍË³ö³ÌÐò
        if( mService.isAvailable() == false ){
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }

        usbCtrl = new UsbController(this,mHandler);
        u_infor = new int[6][2];
        u_infor[0][0] = 0x1CBE;
        u_infor[0][1] = 0x0003;
        u_infor[1][0] = 0x1CB0;
        u_infor[1][1] = 0x0003;
        u_infor[2][0] = 0x0483;
        u_infor[2][1] = 0x5740;
        u_infor[3][0] = 0x0493;
        u_infor[3][1] = 0x8760;
        u_infor[4][0] = 0x0416;
        u_infor[4][1] = 0x5011;
        u_infor[5][0] = 0x0416;
        u_infor[5][1] = 0xAABB;

        btn_conn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                byte isHasPaper;
                    usbCtrl.close();
                    int i = 0;
                    for (i = 0; i < 6; i++) {
                        dev = usbCtrl.getDev(u_infor[i][0], u_infor[i][1]);
                        if (dev != null)
                            break;
                    }

                    if (dev != null) {
                        if (!(usbCtrl.isHasPermission(dev))) {
                            usbCtrl.getPermission(dev);
                        } else {
                            Toast.makeText(getApplicationContext(), "obtaining USB device access permissions success",
                                    Toast.LENGTH_SHORT).show();
                            btnSend.setEnabled(true);
                            btn_conn.setEnabled(false);
                        }
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            byte isHasPaper;
            @Override
            public void onClick(View v) {

                String msg = "";
                String textResult = "";
//                byte[] cmd = new byte[3];
//                cmd[0] = 0x1b;
//                cmd[1] = 0x61;

                if( isHasPaper == 0x38 ){
                    Toast.makeText(getApplicationContext(), "The printer has no paper",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if( CheckUsbPermission() == true ){
                    BluetoothPrintDriver.Begin();
                    String print1DBarcodeStr = etBT.getText().toString();

                    byte[] contents = print1DBarcodeStr.getBytes();
                    byte[] formats  = {(byte) 0x1d, (byte) 0x6b, (byte) 0x49, (byte)contents.length};

                    byte[] bytes    = new byte[formats.length + contents.length];

                    System.arraycopy(formats, 0, bytes, 0, formats.length );
                    System.arraycopy(contents, 0, bytes, formats.length, contents.length);

                    usbCtrl.sendByte(bytes,dev);
                    usbCtrl.sendByte(LineFeed(),dev);

                /*  usbCtrl.sendByte(SetTextSize((byte) ConsBt.DOUBLE_WIDTH), dev);
                    usbCtrl.sendByte(SetTextSize((byte) ConsBt.DOUBLE_HEIGHT), dev);
                    usbCtrl.sendByte(SetAlign((byte) ConsBt.ALIGN_CENTER), dev);
                    usbCtrl.sendMsg("DAYTRANS TRAVEL", "GBK", dev);
//                    usbCtrl.sendByte(LineFeed(), dev);*/
//
//                    byte[] formats  = {(byte) 0x1d, (byte) 0x6b, (byte) 73,(byte) 0x0d};
//                    byte[] nullvar  = {(byte) 0x00};
//                    byte[] contents = content.getBytes();
//
//                    byte[] bytes    = new byte[formats.length + contents.length];
//                    System.arraycopy(formats, 0, bytes, 0, formats.length );
//                    System.arraycopy(contents, 0, bytes, formats.length, contents.length);




//                    byte[] print_Barcode = new byte[4 + contents.length + 1];
//                    print_Barcode[0] = 0x1d;
//                    print_Barcode[1] = 0x6b;
//                    print_Barcode[2] = 73;
//                    print_Barcode[3] = (byte) contents.length;
//                    System.arraycopy(contents, 0, print_Barcode, 4, contents.length);
//                    usbCtrl.sendByte(BluetoothPrintDriver.SetHRIUsb(), dev);
//                    usbCtrl.sendByte(BluetoothPrintDriver.BarcodeHeightUsb(), dev);
//                    usbCtrl.sendByte(BluetoothPrintDriver.BarcodeWidthUSB(), dev);

//                    byte[] bytes2 = new byte[bytes.length + nullvar.length ];
//                    System.arraycopy(bytes, 0, bytes2, 0, bytes.length );
//                    System.arraycopy(nullvar, 0, bytes2, bytes.length, nullvar.length);




                    /*usbCtrl.sendByte(SetTextSize((byte) ConsBt.CANCEL), dev);
                    usbCtrl.sendByte(SetAlign((byte) ConsBt.ALIGN_LEFT), dev);
                    usbCtrl.sendMsg("Untuk di berikan kepada petugas SPBU", "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);

                    usbCtrl.sendMsg("MNFGRG1601268923", "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);

                    usbCtrl.sendMsg("Untuk Unit :  8101 ", "GBK", dev);
                    usbCtrl.sendMsg("Kode Sopir :  D7970AJ ", "GBK", dev);
                    usbCtrl.sendMsg("Nama Sopir :  ADE KUSWATA ", "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);

                    usbCtrl.sendMsg("Voucher Pengisian BBM", "GBK", dev);
                    usbCtrl.sendMsg("SPBU       :  SPBU 57 ", "GBK", dev);
                    usbCtrl.sendMsg("Jenis      :  Solar ","GBK",dev);
                    usbCtrl.sendMsg("Volume     :  5 Liter ", "GBK", dev);
                    usbCtrl.sendMsg("Jumlah     :  Rp. 28.250 ", "GBK", dev);
                    usbCtrl.sendMsg("Petugas    :  DEMO ", "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);
                    usbCtrl.sendByte(LineFeed(), dev);

                    String Title = String.format("%-5s %5s %10s\n", "Checker", "Sopir", "Peg.SPBU");
                    usbCtrl.sendMsg(Title, "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);
                    usbCtrl.sendByte(LineFeed(), dev);

                    String Isi = String.format("%-5s %5s %10s\n", "(DEMO)", "(ADE KUSWATA)", "(....)");
                    usbCtrl.sendMsg(Isi,"GBK",dev);

                    usbCtrl.sendByte(SetAlign((byte) ConsBt.ALIGN_CENTER), dev);
                    usbCtrl.sendMsg("-Terima Kasih-\n2016-01-26 15:07:12", "GBK", dev);
                    usbCtrl.sendByte(LineFeed(), dev);
                    usbCtrl.sendByte(LineFeed(), dev);
                    usbCtrl.sendByte(LineFeed(), dev);
                    usbCtrl.sendByte(LineFeed(), dev);*/

                }
            }
        });

        bt_btn_conn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serverIntent = new Intent(MainActivity.this,DeviceListActivity.class);
                startActivityForResult(serverIntent,REQUEST_CONNECT_DEVICE_SECURE);
            }
        });
        bt_btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String print1DBarcodeStr = etBT.getText().toString();


                byte[] contents = print1DBarcodeStr.getBytes();
                byte[] formats  = {(byte) 0x1d, (byte) 0x6b, (byte) 0x49, (byte)contents.length};

                byte[] bytes    = new byte[formats.length + contents.length];

                System.arraycopy(formats, 0, bytes, 0, formats.length );
                System.arraycopy(contents, 0, bytes, formats.length, contents.length);

                mService.write(bytes);
                mService.write(LineFeed());

//                String content  = "S";
//
//                byte m = 73;
//
//                byte[] formats  = {(byte) 0x1d, (byte) 0x6b, (byte)m,(byte) 0x0d};
//                byte[] nullvar  = {(byte) 0x00};
//                byte[] contents = content.getBytes();
//
//                byte[] bytes    = new byte[formats.length + contents.length];
//                System.arraycopy(formats, 0, bytes, 0, formats.length );
//                System.arraycopy(contents, 0, bytes, formats.length, contents.length);
//
//                mService.write(bytes);
//
//                mService.write(LineFeed());

//                if(BluetoothPrintDriver.IsNoConnection()){
//                    return;
//                }
//                BluetoothPrintDriver.Begin();


//
//                BluetoothPrintDriver.Begin();
//                BluetoothPrintDriver.SetHRI();
//                BluetoothPrintDriver.BarcodeHeight();
//                BluetoothPrintDriver.BarcodeWidth();
//                BluetoothPrintDriver.AddCodePrint(BluetoothPrintDriver.Code128_B, print1DBarcodeStr);
//                BluetoothPrintDriver.excute();
//                BluetoothPrintDriver.ClearData();
            }


        });
    }
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbController.USB_CONNECTED:
                    Toast.makeText(getApplicationContext(), "permission",
                            Toast.LENGTH_SHORT).show();
                    btnSend.setEnabled(true);
                    btn_conn.setEnabled(false);
                    break;
                default:
                    break;
            }
        }
    };

    public boolean CheckUsbPermission(){
        if( dev != null ){
            if( usbCtrl.isHasPermission(dev)){
                return true;
            }
        }
        btnSend.setEnabled(false);
        btn_conn.setEnabled(true);
        Toast.makeText(getApplicationContext(), "gagal",
                Toast.LENGTH_SHORT).show();
        return false;
    }

    private final  Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:   //ÒÑÁ¬½Ó
                            Toast.makeText(getApplicationContext(), "Connect successful",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case BluetoothService.STATE_CONNECTING:  //ÕýÔÚÁ¬½Ó
                            Log.d("À¶ÑÀµ÷ÊÔ","ÕýÔÚÁ¬½Ó.....");
                            break;
                        case BluetoothService.STATE_LISTEN:     //¼àÌýÁ¬½ÓµÄµ½À´
                        case BluetoothService.STATE_NONE:
                            Log.d("À¶ÑÀµ÷ÊÔ", "µÈ´ýÁ¬½Ó.....");
                            break;
                    }
                    break;
                case BluetoothService.MESSAGE_CONNECTION_LOST:    //À¶ÑÀÒÑ¶Ï¿ªÁ¬½Ó
                    Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothService.MESSAGE_UNABLE_CONNECT:     //ÎÞ·¨Á¬½ÓÉè±¸
                    Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:      //ÇëÇó´ò¿ªÀ¶ÑÀ
                if (resultCode == Activity.RESULT_OK) {   //À¶ÑÀÒÑ¾­´ò¿ª
                    Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
                } else {                 //ÓÃ»§²»ÔÊÐí´ò¿ªÀ¶ÑÀ
                    finish();
                }
                break;
            case  REQUEST_CONNECT_DEVICE:     //ÇëÇóÁ¬½ÓÄ³Ò»À¶ÑÀÉè±¸
                if (resultCode == Activity.RESULT_OK) {   //ÒÑµã»÷ËÑË÷ÁÐ±íÖÐµÄÄ³¸öÉè±¸Ïî
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //»ñÈ¡ÁÐ±íÏîÖÐÉè±¸µÄmacµØÖ·
                    con_dev = mService.getDevByMac(address);

                    mService.connect(con_dev);
                }
                break;
        }
    }

//        switch (requestCode) {
//            case REQUEST_CONNECT_DEVICE_SECURE:
//                // When DeviceListActivity returns with a device to connect
//                if (resultCode == Activity.RESULT_OK)
//                {
//                    // ������һ���豸֮ǰ�ȹر��������������豸֮���л�ʱ�����
//                    BluetoothPrintDriver.close();
//                    // ��ȡ�豸 MAC address
//                    SelectedBDAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
//                    // ��������
//                    if(!BluetoothPrintDriver.OpenPrinter(SelectedBDAddress))
//                    {
//                        BluetoothPrintDriver.close();
//
////	            		mTitle.setText("����ʧ��");
//                        return;
//                    }
//                    else
//                    {
//                        // ���ӳɹ�����ʾ�豸��MAC��ַ
////	            		mTitle.setText(SelectedBDAddress);
//                        Toast.makeText(getApplicationContext(),"Berhasil",Toast.LENGTH_LONG).show();
//                    }
//                }
//                break;
//            case REQUEST_CONNECT_DEVICE_INSECURE:
//                // When DeviceListActivity returns with a device to connect
//                if (resultCode == Activity.RESULT_OK) {
//                    ;//                connectDevice(data, false);
//                }
//                break;
//        }
//    }
}
