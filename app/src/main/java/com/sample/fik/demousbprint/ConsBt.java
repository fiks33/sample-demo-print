package com.sample.fik.demousbprint;

/**
 * Created by Default on 27-Jan-16.
 */
public class ConsBt {

    //Text Attrib
    public static final int BOLD = 0x31;
    public static final int CANCEL = 0x00;
    public static final int DOUBLE_HEIGHT = 0x10;
    public static final int DOUBLE_WIDTH = 0x20;

    //TextAlign
    public static final int ALIGN_LEFT = 0x30;
    public static final int ALIGN_CENTER = 0x31;
    public static final int ALIGN_RIGHT = 0x32;

}
